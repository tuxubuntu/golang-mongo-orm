package mongo

import "log"
import "gopkg.in/mgo.v2"
import "gopkg.in/mgo.v2/bson"

func hErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type Model bson.M

type Storage struct {
	conn *mgo.Session
}

type Collection struct {
	name string
	c *mgo.Collection
}

type Stored interface {
	SetId(string)
	GetId() string
	BuildModel() Model
	ParseModel(Model)
}


func Connect(address string) (*Storage) {
	log.Printf("MongoDB > Use DataBase: %s", address)
	session, err := mgo.Dial(address)
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	self := Storage{
		conn: session,
	}
	return &self
}

func(self *Storage) Collection(db string, col string) (*Collection) {
	log.Printf("Storage > Use Collection: %s", col)
	c := self.conn.DB(db).C(col)
	a := Collection{
		c: c,
		name: col,
	}
	return &a
}

func(self *Storage) Close() () {
	log.Printf("Storage > Close")
	self.conn.Close()
}


func(self *Collection) Create(source Stored) (string) {
	log.Printf("Collection > Create: %s", self.name)
	model := bson.M(source.BuildModel())
	id := bson.NewObjectId()
	id_string := id.Hex()
	model["_id"] = id
	log.Printf("\t-> %v", model)
	err := self.c.Insert(model)
	hErr(err)
	return id_string
}

func(self *Collection) Update(source Stored) {
	log.Printf("Collection > Update: %s", self.name)
	model := bson.M(source.BuildModel())
	id := bson.ObjectIdHex(source.GetId())
	model["_id"] = id
	log.Printf("\t-> %v", model)
	err := self.c.Update(bson.M { "_id": id }, model)
	hErr(err)
}

func(self *Collection) Save(source Stored) {
	if len(source.GetId()) == 0 {
		source.SetId(self.Create(source))
	} else {
		self.Update(source)
	}
}

func(self *Collection) Read(source Stored) {
	log.Printf("Collection > Read: %s", self.name)
	id := bson.ObjectIdHex(source.GetId())
	log.Printf("\t-> %v", id)
	var result bson.M
	err := self.c.FindId(id).One(&result)
	log.Printf("\t-> %v", result)
	source.ParseModel(Model(result))
	hErr(err)
}
